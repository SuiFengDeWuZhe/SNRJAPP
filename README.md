# snrjapp

> A Vue.js project

## Build Setup

``` bash
# 安装依赖
npm install

# 热加载服务 localhost:8080
npm run dev

# 为生产和缩小制造。
npm run build

# 构建用于生产和查看bundle analyzer报告。
npm run build --report
```

关于如何运作的详细解释, 查看 [guide](http://vuejs-templates.github.io/webpack/) 和 [docs for vue-loader](http://vuejs.github.io/vue-loader).
#模块分析

1.首页模块

    - 首页
        - 轮播图
        - 选项 1.通知
        - 每个通知的页面
    - 通知

2.课表模块
    
    - 课表
        - 每个课的详情框

3.发现模块
    
    - 发现
        - 沈农机器人
        - 校历,日历
        - 问卷调查
        - 流量查询
        - 游戏
        - 考表
        - 成绩
    - 添加

4.设置模块

    - 设置
    - 更换头像
    - 自定义课表背景
    - 关于我们
    - 意见反馈
    - 版本
    - 退出登录

5.其他模块
    - 登录
    - 网络不通畅模块()
        - 下载不下来数据 ajax
        - 
    - 页面加载发生错误上传模块

# 项目开发人员

马春宁、王兴龙
