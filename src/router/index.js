import Vue from 'vue'
import Router from 'vue-router'

import login from '@/components/components/login'
import mainbody from '@/components/mainbody'
import setting from './modules/setting'
import course from './modules/course'
import tools from './modules/tools'
import modules from './modules/modules'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: login
    },
    {
      path:'/main',
      name:'mainbody',
      component:mainbody,
      children:[
        setting,
        course,
        tools
      ]
    },
    modules
  ]
})
