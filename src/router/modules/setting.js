import settingview from '@/components/components/setting'
import setting from '@/components/components/setting/setting'
// import aboutus from '@/components/components/setting/aboutus'
// import feedback from '@/components/components/setting/feedback'

export default {
  path: 'setting',
  component: settingview,
  children: [
    {
      path: '',
      name: '设置',
      component: setting
    }
  ]
}
