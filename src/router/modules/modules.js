import modulesView from '@/components/components/tools/views/modulesView'
import notices from '@/components/components/tools/components/notices'
import noticedetail from '@/components/components/tools/components/noticedetail'
import grades from '@/components/components/tools/components/grades'
import tests from '@/components/components/tools/components/tests'
import more from '@/components/components/tools/components/more'
import iframeView from '@/components/components/tools/views/iframeView'
import aboutus from '@/components/components/setting/aboutus';
import feedback from '@/components/components/setting/feedback';

export default {
  path: '/modules',
  component: modulesView,
  children: [
    {
      path: 'notices',
      name: '通知',
      component: notices
    },
    {
      path: 'noticedetail/:id',
      name: '通知详情',
      component: noticedetail
    },
    {
      path: 'grades',
      name: '成绩',
      component: grades
    },
    {
      path: 'tests',
      name: '考表',
      component: tests
    },
    {
      path: 'more',
      name: '所有模块',
      component: more
    },
    {
      path: 'iframe/:name/:id',
      name: '模块',
      component: iframeView
    },
    {
      path: 'aboutus',
      name: '关于我们',
      component: aboutus
    },
    {
      path: 'feedback',
      name: '意见反馈',
      component: feedback
    }
  ]
}
