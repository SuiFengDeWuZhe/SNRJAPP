import Vue from 'vue'
import Vuex from 'vuex'

import info from './modules/info'
import courses from './modules/courses'
import network from './modules/network'
import notices from './modules/notices'
import tests from './modules/tests'
import chajian from './modules/chajian'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    info,
    courses,
    network,
    notices,
    tests,
    chajian
  }
})
