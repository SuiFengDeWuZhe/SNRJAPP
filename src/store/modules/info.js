export default {
  state: {
    account: '',
    pwd: '',
    detail: {},
    openDate: '2018-1-26',
    navIndex: 'course',
    isReload: false
  },
  mutations: {
    setUserAccount (state, info) {
      state.account = info.account
      state.pwd = info.pwd
    },
    setUserInfo (state, info) {
      state.detail = info
    },
    setOpenDate (state, d) {
      state.openDate = d;
    },
    setNavIndex (state, index){
      state.navIndex = index;
    },
    setIsReload (state, isReload) {
      state.isReload = isReload;
    }
  },
  getters: {
    getUserAccount (state) {
      return {
        account: state.account,
        pwd: state.pwd
      }
    },
    getUserInfo (state) {
      return state.detail;
    },
    getOpenDate (state) {
      return state.openDate;
    },
    getNavIndex(state){
      return state.navIndex;
    }
  }
}
