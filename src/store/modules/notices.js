export default {
  state: {
    list: []
  },
  mutations: {
    setNotices (state, notices) {
      state.list = notices;
    }
  },
  getters: {
    getNotices (state) {
      return state.list;
    }
  }
}
