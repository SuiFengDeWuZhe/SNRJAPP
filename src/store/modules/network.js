export default {
  state: {
    schoolNetwork: 'online',
    outNetwork: 'online'
  },
  mutations: {
    setNetworkStatus(state, info) {
      // console.log('state', state);
      state[info.name] = info.status;
      localStorage.setItem('network', state);
    }
  },
  getters: {
    getNetworkStatus(state) {
      return state;
    }
  }
}
