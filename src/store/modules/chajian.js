export default {
  state: {
    list: []
  },
  mutations: {
    setChajian (state, list) {
      state.list = list;
    }
  },
  getters: {
    getChajian (state) {
      return state.list;
    }
  }
}
