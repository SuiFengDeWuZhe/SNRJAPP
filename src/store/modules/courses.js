export default {
  state: {
    courses: []
  },
  mutations: {
    setCourses (state, courses) {
      state.courses = courses;
    }
  },
  getters: {
    getCourses (state) {
      return state.courses;
    }
  }
}
