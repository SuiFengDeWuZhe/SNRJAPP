export default {
  state: {
    list: []
  },
  mutations: {
    setTests (state, tests) {
      state.list = tests;
    }
  },
  getters: {
    getTests (state) {
      return state.list;
    }
  }
}
