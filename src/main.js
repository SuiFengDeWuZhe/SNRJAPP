// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'babel-polyfill';
import 'url-search-params';
import Vue from 'vue';
import MintUI from 'mint-ui';
import 'mint-ui/lib/style.css';
import App from './App';
import router from './router';
import store from './store';
// import './libs/network';
import addFetchMiddleware from '@/libs/fetchMiddleware';

Vue.use(MintUI)

Vue.config.productionTip = false

window.APP_CONFIG = {
  version: '3.1.0',
  isDebug: false
}
let fromapp = window.location.search.match(/fromapp=(\d)/);
if(fromapp && fromapp[1] === '0'){
  window.APP_CONFIG.isDebug = true;
}
addFetchMiddleware();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
