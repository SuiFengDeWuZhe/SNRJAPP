import encrypt from './encrypt'
import utils from './utils'

let keyname = 'wssYasUUWpySsYKeY6T7w==';

let key = localStorage.getItem(keyname);
if(!key){
  localStorage.setItem(keyname, utils.getUID(16));
}

export default {
  setItem(key, text){
    let aesKey = localStorage.getItem(keyname);
    localStorage.setItem(encrypt.AES(key, aesKey), encrypt.AES(JSON.stringify(text), aesKey));
  },
  getItem(key){
    let aesKey = localStorage.getItem(keyname);
    let aesData = localStorage.getItem(encrypt.AES(key, aesKey));
    let backData = null;
    if(aesData){
      backData = JSON.parse(encrypt.DAES(aesData, aesKey));
    }
    return backData;
  },
  removeItem(key){
    let aesKey = localStorage.getItem(keyname);
    localStorage.removeItem(encrypt.AES(key, aesKey));
  },
  clear(){
    localStorage.clear();
    localStorage.setItem(keyname, utils.getUID(16));
  }
}
