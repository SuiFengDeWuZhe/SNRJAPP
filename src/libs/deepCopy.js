/*
 * 对象，数组深拷贝方法封装 作者：马春宁
 */

function deepCopy(obj) {
  var objArray = Array.isArray(obj) ? [] : {};
  for (var key in obj) {
    objArray[key] = typeof obj[key] === 'object' ? deepCopy(obj[key]) : obj[key];
  }
  return objArray;
}
