// 课程信息处理 作者：马春宁
// 对数据中的week进行处理

// 对数据里的空格和*进行处理
function cleaning(data) {
  var datalen = data.length;
  for (var i = 0; i < datalen; i++) {
    for (var key in data[i]) {
      data[i][key] = data[i][key].replace(/(\s|\*)/g, "");
    }
  }
  return cnTonum(data);
}
// 对数据中的jieCi项进行处理 jieCi:'一' ＝> jieCi:'1'
function cnTonum(data) {
  var section = ["一", "二", "三", "四", "五", "六"];
  var datalen = data.length;
  for (var i = 0; i < datalen; i++) {
    data[i].jieCi = section.indexOf(data[i].jieCi) + 1;
  }
  return changeWeek(data);
}
// 对数据中的week属性进行处理
// week:‘1|2|3|4|5|6|7|8’=>week:[1,2,3,4,5,6,7,8]
function changeWeek(data) {
  var datalen = data.length;
  for (var i = 0; i < datalen; i++) {
    data[i].week = data[i].week.substring(1, data[i].week.length - 1);
    data[i].week = data[i].week.split("|");
    for(var p in data[i].week){
      data[i].week[p] = parseInt(data[i].week[p]);
    }
  }
  return splitName(data);
}
// 对数据中的courseName进行处理
// '毛泽东思想和中国特色社会主义理论体系概论' 只取前10个字
function splitName(data) {
  var datalen = data.length;
  for(var i = 0; i < datalen; i++){
    if(data[i].courseName.length > 10){
      data[i].courseName = data[i].courseName.substring(0,10);
    }
  }
  return splitBuilding(data);
}
// 对数据中的building进行处理
// '第一教学楼'=>'一教'
function splitBuilding(data) {
  var datalen = data.length;
  var dataVal = ['第一教学楼','第二教学楼'];
  for(var i = 0; i < datalen; i++){
    if(data[i].building.indexOf(dataVal)){
      data[i].building = data[i].building.substring(1,3);
    }
  }
  return data;
}
export default { cleaning };
