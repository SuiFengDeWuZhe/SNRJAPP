import {HOST_SCHOOL, HOST_OUT} from '@/config/ip';
/*
 * 用于获取UUID,生成不重复的字符串，
 * 可以传入一个数字参数，用于表示要生成字符串的长度，
 * (ps:当传入的数字越小生成的字符串重复的概率越大)
 */
function getUID(len, radix) {
  let chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
  let uuid = [];
  let i;
  radix = radix || chars.length;

  if (len) {
    // Compact form
    for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random() * radix];
  } else {
    // 参考rfc4122，https://tools.ietf.org/html/rfc4122
    let r;

    uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
    uuid[14] = '4';

    for (i = 0; i < 36; i++) {
      if (!uuid[i]) {
        r = 0 | Math.random() * 16;
        uuid[i] = chars[(i === 19) ? (r & 0x3) | 0x8 : r];
      }
    }
  }

  return uuid.join('');
}
/*
 * 用于判断js中的数据类型，传入要判断的变量，返回该变量的类型
 */
function getType(variety) {
  const map = {
    '[object Boolean]': 'boolean',
    '[object Number]': 'number',
    '[object String]': 'string',
    '[object Function]': 'function',
    '[object Array]': 'array',
    '[object Date]': 'date',
    '[object RegExp]': 'regExp',
    '[object Undefined]': 'undefined',
    '[object Null]': 'null',
    '[object Object]': 'object',
    '[object Arguments]': 'arguments',
    '[object HTMLCanvasElement]': 'canvas'
  };
  // if (typeof window !== 'undefined') {
  //     if (variety instanceof Element) {
  //         return 'Element';
  //     }
  // }
  return map[Object.prototype.toString.call(variety)];
}
/*
 * 获取当前app的网络状态
 */
function getNetworkType(info){
  let type = '';
  if(info.schoolNetwork === 'online'){
    type = 'school';
  } else if(info.outNetwork === 'online'){
    type = 'out';
  } else {
    type = 'none';
  }
  console.log('type',type, info);
  return type;
}
/*
 * 获取当前app的网络状态下，最好的请求地址
 */
function getHost(){
  let host = HOST_SCHOOL;

  let networkInfo = localStorage.getItem('network');
  let networkType = '';

  if(networkInfo){
    networkType = getNetworkType(networkInfo);
    if(networkType === 'out'){
      host = HOST_OUT;
    }
  }
  return host;
}

export default {
  getUID,
  getType,
  getNetworkType,
  getHost
}
