import storage from './storage';

function autoStorage(opts, next){
  let data = storage.getItem(opts.url);
  // console.log('请求数据', data, opts.url);
  if(data && !opts.data.f){
    opts.success && opts.success({
      status:9999,
      statusText:'storage',
      data: {
        code: 0,
        data: data
      }
    })
    next(opts, 'break');
    // console.log('从缓存拿到数据', opts.url);
    return;
  }
  // let loginReg = /\/login/;
  // if(loginReg.test(opts.url)){
  //   storage.setItem('account', opts.data);
  // }
  let f;
  const portArr = ['getCourseInfo', 'getKaoBiao',
    'getScore', 'commitView', 'openingdate'];
  let regs = new RegExp('/(' + portArr.join('|') + ')');
  // console.log(regs.test(opts.url), opts.url, regs.toString());
  if(regs.test(opts.url)){
    f = opts.success;
    opts.success = function(res){
      if(res.data.data){
        storage.setItem(opts.url, res.data.data);
      }
      f && f(res);
    }
  }

  next(opts)
}

export default autoStorage;
